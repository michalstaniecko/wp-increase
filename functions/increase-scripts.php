<?php

function increase_admin_scripts() {
	
}

function increase_dev_scripts() {
	
}
function increase_prod_scripts() {

}

add_action('admin_enqueue_scripts', 'increase_admin_scripts');

if (defined('__ENV__') && __ENV__ == 'dev') {
	add_action('wp_enqueue_scripts', 'increase_dev_scripts'); 
} else {
	add_action('wp_enqueue_scripts', 'increase_prod_scripts');
}

?>